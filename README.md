# Language files for connecting to Weblate

## Motivation

The internationalization of the MintApps is implemented in a component-oriented manner. This means that every .vue file has a `<i18n>` tag included at the end, which imports a JSON file from the `locales` folder.

Each of these files contains *all* languages for *exactly one* component, e.g. `FranckHertz.json`:

~~~
{
  "de": {
    "acceleration-voltage": "Beschleunigungsspannung *U*~A~",
    "ampere-meter": "Amperemeter",
    "ampere-meter-0": "keines",
    ...
  },
  "en": {
    "acceleration-voltage": "Acceleration Voltage *U*~A~",
    "ampere-meter": "Ampere meter",
    "ampere-meter-0": "none",
    ...
  }
}
~~~

As the example shows, the language split occurs at the top level of the JSON file.

Unfortunately weblate only supports language files for *exactly one* language. This repository serves as a bridge between the "two worlds". The JSON files for weblate contain *all* components for only *one* language per file, e.g. `mint-de.json`:

~~~
{
  "Index": {
    "subtitle": "Eine bunte Sammlung an Spielen, Applikationen und Simulationen zu naturwissenschaftlichen Fächern.",
    "button": "Über diese Apps",
    "about-title": "Über die MintApps",
    ...
    },
    "FranckHertz": {
      "acceleration-voltage": "Beschleunigungsspannung *U*~A~",
      "ampere-meter": "Amperemeter",
      "ampere-meter-0": "keines",
      ...
    }
}
~~~

## script

The script `sync.js` serves to automate the change between the two formats. It is started directly from the root directory of the MintApps, which contains the two subdirectories `weblate` and `client`, among other things.

### Installation

~~~
cd weblate
npm install
~~~

### Start

~~~
cd mintapps (replace correct dir for your installation)
node weblate/sync.js
~~~

The synchronization takes place in both directions, i.e. new translations are taken from weblate and missing keys from the locale files are provided for translation in weblate.

**Important:** Before running the script, all changes in Weblate *must* be written into the repo itself, immediately afterwards Weblate *must* read the repo again.

## Synchronization flow in detail

At the top level, the script loops through all modules of the application, currently these are `main` and `mint`. The following steps are carried out for each of these modules:

1. Read all existing weblate files; missing ones are created automatically (if, for example, a new language has been added)

2. Read all existing locale files from client repository

3. Ensure consistency of locale files; missing languages ​​are added, keys that are no longer relevant (which were deleted in the DEFAULT_LANG) are removed

4. Add missing components and keys to weblate files; Contents are automatically taken from the locale files (so that the developer can enter these **initial** there without using the weblate repository)

5. Remove superfluous components from the weblate files, e.g. if a component no longer exists

6. Apply changes from weblate files; if a value already exists in the locale files for the specific key, the user decides manually which value to adopt.

7. Save changes to weblate files

8. Save changes to locale files
